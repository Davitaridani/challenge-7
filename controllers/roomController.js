const { Room } = require("../models");

exports.createRoom = async (req, res) => {
  await Room.create();

  res.send("Room created successfully");
};
