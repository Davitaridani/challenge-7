const passport = require("./utils/passport");
const express = require("express");
const app = express();
const port = 3001;

const auth = require("./routers/auth");
const room = require("./routers/room");
const playGame = require("./routers/play-game");

app.use(express.urlencoded());
app.use(passport.initialize());

app.use(auth);
app.use(room);
app.use(playGame);

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
