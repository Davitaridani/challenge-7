const express = require("express");
const router = express.Router();
const passport = require("../utils/passport");

const playGameController = require("../controllers/playGameController");

router.post(
  "/join-room",
  passport.authenticate("jwt", { session: false }),
  playGameController.playGame
);

module.exports = router;
